#!/bin/sh

# BUILD contains a temporary set of paludis+dependencies
BUILD=$PWD/build

# DEST is the final image
DEST=$PWD/dest
mkdir -p "$DEST"

mkdir -p "$BUILD" "$BUILD"/bin "$BUILD"/lib "$BUILD"/usr "$BUILD"/usr/bin "$BUILD"/usr/lib
ln -s lib "$BUILD"/lib64
ln -s bin "$BUILD"/sbin
ln -s lib "$BUILD"/usr/lib64
ln -s bin "$BUILD"/usr/sbin

PATH=$DEST/bin:$BUILD/bin:$PATH
LD_LIBRARY_PATH=$DEST/lib:$BUILD/lib:$LD_LIBRARY_PATH


# eclectic is a dependency of paludis
pushd eclectic
	./autogen.bash && \
		./configure --prefix="$DEST" && \
		make && \
		make install || \
		exit 1
popd #eclectic

pushd paludis

	cmake \
			-DENABLE_DOCUMENTATION:BOOL=FALSE \
			-DCMAKE_COLOR_MAKEFILE:BOOL=FALSE \
			-DCMAKE_VERBOSE_MAKEFILE:BOOL=FALSE \
			-DCMAKE_BUILD_TYPE:STRING=None \
			-DCMAKE_C_FLAGS:STRING="-march=native -pipe" \
			-DCMAKE_CXX_FLAGS:STRING="-march=native -pipe" \
			-DCMAKE_INSTALL_PREFIX:PATH="$BUILD" \
			-DCMAKE_FIND_ROOT_PATH=/usr \
			-DCMAKE_FIND_ROOT_PATH_MODE_PROGRAM:STRING=NEVER \
			-DCMAKE_SYSTEM_PREFIX_PATH:PATH=/usr \
			-DCMAKE_INSTALL_LIBDIR:STRING=lib \
			-DCMAKE_INSTALL_DATAROOTDIR:PATH="$BUILD/usr/share/" \
			-DENABLE_GTEST:BOOL=FALSE \
			-DENABLE_PBINS:BOOL=FALSE \
			-DPALUDIS_COLOUR_PINK:BOOL=FALSE \
			-DENABLE_PYTHON:BOOL=FALSE \
			-DENABLE_RUBY:BOOL=FALSE \
			-DENABLE_SEARCH_INDEX:BOOL=FALSE \
			-DENABLE_VIM:BOOL=FALSE \
			-DENABLE_XML:BOOL=FALSE \
			-DCMAKE_INSTALL_SYSCONFDIR=/etc \
			-DCMAKE_INSTALL_DOCDIR=/usr/share/doc/paludis \
			-DPALUDIS_VIM_INSTALL_DIR=/usr/share/vim/vimfiles \
			-DPALUDIS_CLIENTS=cave \
			-DPALUDIS_ENVIRONMENTS=default \
			-DPALUDIS_REPOSITORIES=default\;repository \
			-DPALUDIS_DEFAULT_DISTRIBUTION=exherbo \
			-DCONFIG_FRAMEWORK=eclectic \
			. && \
		make && \
		make install || \
		exit 1
popd #paludis

export PALUDIS_DO_NOTHING_SANDBOXY=true


